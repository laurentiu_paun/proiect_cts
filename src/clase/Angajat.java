package clase;

import java.util.*;
import java.text.*;
import java.io.*;
public class Angajat implements Serializable{
    private String nume;
    private String prenume;
    private String cnp;
    private Date dataAngajare;
    private final Date dataCurenta = new Date();
    
    public int AnCuren(){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(dataCurenta);
    	int an = cal.get(1);//1 = Calendar.YEAR
    	return an;
    }
    
    public int AnDataAng(){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(this.dataAngajare);
    	int an = cal.get(1);//1 = Calendar.YEAR
    	return an;
    }
    
    public Angajat(String nume,String prenume,String cnp,Date dataAngajare){
        this.nume = nume;
        this.prenume = prenume;
        this.cnp = cnp;
        this.dataAngajare = dataAngajare;
    }
    
    public String getNume(){
        return this.nume;
    }
    public String getPrenume(){
        return this.prenume;
    }
    public String getCnp(){
        return this.cnp;
    }
    public Date getDataAngajare(){
        return this.dataAngajare;
    }
    
    public void setNume(String s){
        this.nume = s;
    }
    public void setPrenume(String s){
        this.prenume = s;
    }
    public void setCnp(String s){
        this.cnp = s;
    }
    public void setDataAngajare(Date d){
        this.dataAngajare = d;
    }
    
    public String toString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String d = sdf.format(this.getDataAngajare());
        
        return this.nume+" "+this.prenume+" "+this.cnp+" "+d+"";
    }
    
    public boolean equals(Object o){
        if(o == null) return false;
        else if(o instanceof Angajat){
            Angajat a = (Angajat) o;
            return this.cnp.equals(a.cnp);
        }else {
            return false;
        }
    }
    
    //metode adaugate pentru a verifica data -> create dupa verificarea cu JUnit
    public boolean checkDataAngajareMare(){
    	if(this.dataAngajare.compareTo(dataCurenta)>0){
        	return false;
        }else{
        	return true;
        }
    }
    
    public boolean checkDataAngajareMica(){
    	if(AnCuren() - AnDataAng() > 65){
        	return false;
        }else{
        	return true;
        }
    }
}

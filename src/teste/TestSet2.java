package teste;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Angajat;

public class TestSet2 {
	
	public FileReader fReader;
	public BufferedReader BfR;

	@Before
	public void setUp() throws Exception {
		fReader = new FileReader("Fisier");
		BfR = new BufferedReader(fReader);
	}

	@After
	public void tearDown() throws Exception {
		fReader.close();
		BfR.close();		
	}

	//testam citirea din fisier -> data angajare incorecta
	@Test
	public void test11() throws ParseException {
		try {
			String linie = null;
			while((linie = BfR.readLine())!= null && linie.length() != 0){
				boolean rezultat = !linie.split(" ")[0].equals("0");// [0] index-ul de la vectorul facut de split
				Integer data = Integer.parseInt(linie.split(" ")[1]);
				SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				Date dataAng = sdf.parse(data.toString());
				Angajat ang = new Angajat("Dumitru","Ilie","1741014098765",dataAng);
				System.out.println("Data: " + data + " este: " + rezultat);
				assertEquals("Eroare la " + rezultat + " cu " + data, rezultat, ang.checkDataAngajareMare() && ang.checkDataAngajareMica());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

package clase;

public class AngajatInexistentException extends Exception{
    public AngajatInexistentException(){
        super("Nu exista angajatul respectiv");
    }
}

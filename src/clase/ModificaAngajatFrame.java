package clase;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

public class ModificaAngajatFrame extends JFrame{
    private JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    
    private JPanel p4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel p5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    
    private JLabel e1 = new JLabel("Nume:");
    private JTextField t1 = new JTextField(12);
    
    private JLabel e2 = new JLabel("Prenume:");
    private JTextField t2 = new JTextField(12);
    
    
    
    private JLabel e4 = new JLabel("Data angajarii:");
    private JTextField t4 = new JTextField(12);
    
    private JButton b1 = new JButton("OK");
    
    Angajat ang = null;
    
    public ModificaAngajatFrame(Angajat a){
        
        setLayout(new GridLayout(5,1));
        add(p1);add(p2);add(p4);add(p5);
        p1.add(e1);p1.add(t1);
        p2.add(e2);p2.add(t2);
        
        p4.add(e4);p4.add(t4);
        p5.add(b1);
        
        ang = a;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String d = sdf.format(ang.getDataAngajare());
        
        t1.setText(ang.getNume());
        t2.setText(ang.getPrenume());
        t4.setText(d+"");
        
        b1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ev){
                String nume = t1.getText();
                String prenume = t2.getText();
                
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date d = null;
                try{
                    d = sdf.parse(t4.getText());
                }catch(ParseException ex){
                    System.out.println("Data nu a fost introdusa corect");
                }
                
                try{
                    Firma.getInstance().modificaDate(ang,nume,prenume);
                    Firma.getInstance().modificaDataAngajarii(ang,d);
                }catch(AngajatInexistentException ex) {}
                dispose();
            
            }
        
        });
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    
    }

}

package teste;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Angajat;
import clase.Firma;
import static org.mockito.Mockito.*;

public class TestSet3 {

	Firma Dacia;
	Angajat ang;
	
	@Before
	public void setUp() throws Exception {
		Dacia = Firma.getInstance();
		
		ang = mock(Angajat.class);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String data = "08/10/2011";
		Date dataAng = sdf.parse(data);
		
		when(ang.getNume()).thenReturn("Pierre");
		when(ang.getPrenume()).thenReturn("Louis");
		when(ang.getCnp()).thenReturn("1720506456789");
		when(ang.getDataAngajare()).thenReturn(dataAng);
		when(ang.checkDataAngajareMare()).thenReturn(true);
		when(ang.checkDataAngajareMica()).thenReturn(true);
		
		Dacia.adaugaAngajat(ang);
	}

	//verificam daca angajatul mock a fost adaugat
	@Test
	public void test12() {
		ArrayList<Angajat> ang = Dacia.getListaAngajati();
		boolean empty = ang.isEmpty();
		assertEquals(false, empty);
	}
	
	@Test
	public void test13() {
		assertEquals(true, ang.checkDataAngajareMare());
	}
	
	@Test
	public void test14() {
		Date d = ang.getDataAngajare();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dat = sdf.format(d);
		assertEquals("08/10/2011", dat);
	}

}

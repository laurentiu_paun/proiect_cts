package clase;

import java.util.*;
import java.io.*;
public class Firma implements AngajatSubject,Serializable {
    private ArrayList<Angajat> listaAngajati = new ArrayList<Angajat>();
    private ArrayList<AngajatListener> listeners = new ArrayList<AngajatListener>();
    
    private static Firma singleton = null;
    public static Firma getInstance(){
        if(singleton == null)singleton = new Firma();
        return singleton;
    }
    private Firma(){
    
    }
    
    public void addAngajatListener(AngajatListener al){
        listeners.add(al);
    }
    public void notifyAngajatListeners(){
        for(AngajatListener al:listeners){
            al.listaAngajatiModificata();
        }
    }
    
    
    public  ArrayList<Angajat> getListaAngajati(){
        return this.listaAngajati;
    }
    public void setListaAngajati( ArrayList<Angajat> list){
        this.listaAngajati = list;
        notifyAngajatListeners();
    }
    public void adaugaAngajat(Angajat a){
    	if(a.checkDataAngajareMare() && a.checkDataAngajareMica()){
	        listaAngajati.add(a);
	        notifyAngajatListeners();
    	}else{
    		throw new IllegalArgumentException("Data angajarii nu corespunde!");
    	}  	
    }
    public void eliminaAngajat(Angajat a){   
        listaAngajati.remove(a);
        notifyAngajatListeners();
    }
    
    public void modificaDate(Angajat a,String nume,String prenume) throws AngajatInexistentException{
        boolean modificat = false;
        for(Angajat a1:listaAngajati){
            if(a1.equals(a)){
                a.setNume(nume);
                a.setPrenume(prenume);
                modificat = true;
                notifyAngajatListeners();
            }
        }
        if(modificat == false){
            throw  new  AngajatInexistentException();
        }
    }
    public void modificaDataAngajarii(Angajat a,Date d) throws AngajatInexistentException {
        boolean modificat = false;
        for(Angajat a1:listaAngajati){
            if(a1.equals(a)){
                a.setDataAngajare(d);
                modificat = true;
                notifyAngajatListeners();
            }
        }
        if(modificat == false){
            throw  new  AngajatInexistentException();
        }
        
    }
}

package clase;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AdaugaAngajatFrame extends JFrame{
    private JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel p3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel p4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel p5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    
    private JLabel e1 = new JLabel("Nume:");
    private JTextField t1 = new JTextField(12);
    
    private JLabel e2 = new JLabel("Prenume:");
    private JTextField t2 = new JTextField(12);
    
    private JLabel e3 = new JLabel("CNP:");
    private JTextField t3 = new JTextField(12);
    
    private JLabel e4 = new JLabel("Data angajarii:");
    private JTextField t4 = new JTextField(12);
    
    private JButton b1 = new JButton("OK");
    
    public AdaugaAngajatFrame(){
        
        setLayout(new GridLayout(5,1));
        add(p1);add(p2);add(p3);add(p4);add(p5);
        p1.add(e1);p1.add(t1);
        p2.add(e2);p2.add(t2);
        p3.add(e3);p3.add(t3);
        p4.add(e4);p4.add(t4);
        p5.add(b1);
        
        b1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ev){
                String nume = t1.getText();
                String prenume = t2.getText();
                String cnp = t3.getText();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date d = null;
                try{
                    d = sdf.parse(t4.getText());
                }catch(ParseException ex){
                    System.out.println("Data nu a fost introdusa corect");
                }
                Firma.getInstance().adaugaAngajat(new Angajat(nume,prenume,cnp,d));
                dispose();
            
            }
        
        });
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    
    }

}
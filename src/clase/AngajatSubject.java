package clase;

public interface AngajatSubject{
    public void addAngajatListener(AngajatListener al);
    public void notifyAngajatListeners();
}

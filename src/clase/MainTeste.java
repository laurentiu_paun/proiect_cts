package clase;

import junit.framework.TestSuite;
import junit.textui.TestRunner;
import teste.TestSet1;

public class MainTeste {

	public static void main(String[] args) {
		TestSuite testare = new TestSuite();
		  testare.addTest(new TestSet1("test3"));
		  testare.addTest(new TestSet1("test6"));
		  testare.addTest(new TestSet1("test8"));
		  testare.addTest(new TestSet1("test9"));
		  TestRunner.run(testare);
	}

}

package clase;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class MainFrame extends JFrame{
    private DefaultListModel model = new DefaultListModel();
    private JList<Angajat> list= new JList<Angajat>(model);
    private JScrollPane jsp = new JScrollPane(list);
    
    
    private JMenuBar mb = new JMenuBar();
    private JMenu m1 = new JMenu("Optiuni");
    private JMenuItem mi1 = new JMenuItem("Salveaza angajati");
    private JMenuItem mi2 = new JMenuItem("Importa angajati");
    
    private JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    
    private JButton b1 = new JButton("Adauga angajat");
    private JButton b2 = new JButton("Modifica angajat");
    private JButton b3 = new JButton("Sterge angajat");
    
    private JLabel e1 = new JLabel("NR ANGAJATI: 0 ");
    
    private static MainFrame singleton = null;
    public static MainFrame getInstance(){
        if(singleton == null) singleton  = new MainFrame();
        return singleton;
    }
    
    private MainFrame(){
        setJMenuBar(mb);
        mb.add(m1);
        m1.add(mi1);m1.add(mi2);
        
        add(jsp);
        add(p1,BorderLayout.NORTH);
        p1.add(b1);p1.add(b2);p1.add(b3);
        add(p2,BorderLayout.SOUTH);
        p2.add(e1);
        
        new Thread(){
            public void run(){
                while(true){
                    try{
                        Thread.sleep(1500);
                        e1.setText("NR ANGAJATI: "+Firma.getInstance().getListaAngajati().size()+"");
                    }catch(Exception ex){};
                }
            }
        
        }.start();
        
        Firma.getInstance().addAngajatListener(new AngajatListener(){
            public void listaAngajatiModificata(){
                afisareAngajati();
            }
        
        });
        
        mi1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ev){
                try{
                  JFileChooser chooser = new JFileChooser();
                  int r = chooser.showSaveDialog(null);
                  if(r == JFileChooser.APPROVE_OPTION){
                    File file = chooser.getSelectedFile();
                    ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
                    out.writeObject(Firma.getInstance().getListaAngajati());
                    out.close();
                  } 
                
                }catch(IOException ex){};
                
            }
        
        });
        
        mi2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ev){
                try{
                  JFileChooser chooser = new JFileChooser();
                  int r = chooser.showOpenDialog(null);
                  if(r == JFileChooser.APPROVE_OPTION){
                    File file = chooser.getSelectedFile();
                    ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
                    ArrayList<Angajat> lst = null;
                    try{
                        lst = (ArrayList<Angajat>) in.readObject();
                    }catch(Exception ex){};
                    Firma.getInstance().setListaAngajati(lst);
                    Firma.getInstance().notifyAngajatListeners();
                    in.close();
                  } 
                
                }catch(IOException ex){};
                
            }
        });
        
        b1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ev){
                AdaugaAngajatFrame f = new AdaugaAngajatFrame();
                f.setVisible(true);
                
            }
        
        });
        
        b2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ev){
                Angajat a = (Angajat) list.getSelectedValue();
                ModificaAngajatFrame f = new ModificaAngajatFrame(a);
                f.setVisible(true);
                
            }
        
        });
        
        b3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ev){
                Angajat a = (Angajat) list.getSelectedValue();
                Firma.getInstance().eliminaAngajat(a);
                
            }
        
        });
        setSize(500,500);
        setLocationRelativeTo(null);
        setVisible(true);
        
        
        
  }
  public void afisareAngajati(){
      model.clear();
      for(Angajat a:Firma.getInstance().getListaAngajati()){
          model.addElement(a);
      }
  }
  
  public static void main(String [] args){
      new MainFrame();
  }
}
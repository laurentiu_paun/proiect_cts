package teste;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Angajat;
import clase.Firma;

public class TestSet1 extends TestCase {

	public TestSet1(String test) {
		super(test);
	}

	public Firma Dacia = Firma.getInstance();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	//testam ca lista cu angajati din clasa Firma se initializeaza corect si la inceput este goala:
	@Test
	public void test1() {
		ArrayList<Angajat> ang = Dacia.getListaAngajati();
		boolean empty = ang.isEmpty();
		assertEquals(true, empty);
	}
	
	//testam ca lista nu mai este goala dupa ce adaugam un element
	@Test
	public void test2() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/2010";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			Dacia.adaugaAngajat(a);
			ArrayList<Angajat> ang = Dacia.getListaAngajati();
			boolean empty = ang.isEmpty();
			assertEquals(false, empty);
		} catch (Exception e) {
				e.printStackTrace();
		}
	}
	
	//testam ca data angajarii angajatului sa fie anterioara zilei curente:
	@Test
	public void test3() {
		Date dataCurenta = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/2010";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			if(newDate.compareTo(dataCurenta)>0){
				fail("Data angajare setata dupa data curenta!");
			}
		} catch (Exception e) {
				e.printStackTrace();
		}
	}
	
	//verificam metoda nou adaugata checkDataAngajareMare()
	@Test
	public void test4() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/2011";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			assertTrue(a.checkDataAngajareMare());
		} catch (Exception e) {
				e.printStackTrace();
		}
	}
	
	//verificam daca este adaugat in lista un angajat cu data de angajare > data curenta sau lista ramane goala
	@Test
	public void test5() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/2018";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			Dacia.adaugaAngajat(a);
			ArrayList<Angajat> ang = Dacia.getListaAngajati();
			int nr = ang.size();
			assertEquals(0, nr);
		} catch (Exception e) {
		}
	}
	
	//verificam intrarea pe exceptie daca este adaugat in lista un angajat cu data de angajare > data curenta
	@Test
	public void test6() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/2017";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			Dacia.adaugaAngajat(a);
			ArrayList<Angajat> ang = Dacia.getListaAngajati();
			int nr = ang.size();
			if(nr > 0) fail("Nu trebuia sa intre aici!");
		} catch (Exception e) {
		}
	}
	
	//Testam ca angajatul are varsta < 65 ani (este apt de munca)
	@Test
	public void test7() {
		int anCurent = Calendar.getInstance().get(1);//1 = Calendar.YEAR
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/1980";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			Calendar cal = Calendar.getInstance();
		    cal.setTime(a.getDataAngajare());
		    int anAng = cal.get(1);
			if(anCurent - anAng > 65){
				fail("Angajatul are varsta de pensionare!");
			}
		} catch (Exception e) {
				e.printStackTrace();
		}
	}
	
	//testam metoda nou adaugata checkDataAngajareMica()
	@Test
	public void test8() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/2010";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			assertTrue(a.checkDataAngajareMica());
		} catch (Exception e) {
				e.printStackTrace();
		}
	}
	
	//verificam daca este adaugat in lista un angajat cu data de angajare > data curenta sau lista ramane goala
	@Test
	public void test9() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/1900";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			Dacia.adaugaAngajat(a);
			ArrayList<Angajat> ang = Dacia.getListaAngajati();
			int nr = ang.size();
			assertEquals(0, nr);
		} catch (Exception e) {
		}
	}
	
	//verificam intrarea pe exceptie daca este adaugat in lista un angajat cu varsta > 65 ani
	@Test
	public void test10() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = "04/10/1900";
		try {
			Date newDate = sdf.parse(strDate);
			Angajat a = new Angajat("Popescu", "Gica", "1820408123456", newDate);
			Dacia.adaugaAngajat(a);
			ArrayList<Angajat> ang = Dacia.getListaAngajati();
			int nr = ang.size();
			if(nr > 0) fail("Nu trebuia sa intre aici!");
		} catch (Exception e) {
		}
	}

}
